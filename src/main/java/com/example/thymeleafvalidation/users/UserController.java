package com.example.thymeleafvalidation.users;

import com.example.thymeleafvalidation.models.entities.User;
import com.example.thymeleafvalidation.models.requests.UserForm;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
@AllArgsConstructor
public class UserController implements WebMvcConfigurer {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;


    @GetMapping
    public ModelAndView index() {
        ModelAndView mv = new ModelAndView("index");
        return mv;
    }

    @GetMapping("/view/users")
    public String getAllUsers(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "pages/users";
    }

    @GetMapping("/register")
    public ModelAndView registrationPage() {
        ModelAndView mv = new ModelAndView("pages/registration");
        mv.addObject("userRequestForm", new UserForm());
        return mv;
    }

    @PostMapping("/new/user")
    public String register(
            @ModelAttribute @Valid UserForm userForm,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            return "pages/registration";
        }
        User user = modelMapper.map(userForm, User.class);
        System.out.println("new user is " + user);
        userRepository.save(user);
        return "redirect:/view/users";
    }
}
