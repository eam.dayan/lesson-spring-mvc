package com.example.thymeleafvalidation.users;

import com.example.thymeleafvalidation.models.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * This interface is for database access <br>
 * Don't worry about it in this lesson
 */
@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
}