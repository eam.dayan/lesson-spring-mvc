package com.example.thymeleafvalidation.models.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * This is a POJO used to display data from the database table to the user <br>
 * Notice: we do not send the password to the user, because we don't want others to see it.
 */
@Data
public class UserDto implements Serializable {
    private String id;
    private String firstname;
    private String lastname;
    private String email;
}