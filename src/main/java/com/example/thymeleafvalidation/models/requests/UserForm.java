package com.example.thymeleafvalidation.models.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserForm implements Serializable {

    @NotEmpty(message = "firstname is required...")
    @Size(min = 5, max = 50)
    private String firstname;

    @NotEmpty(message = "lastname is required...")
    private String lastname;

    @NotEmpty(message = "email is required...")
    @Email
    private String email;

    @NotEmpty(message = "password is required...")
    private String password;
}
